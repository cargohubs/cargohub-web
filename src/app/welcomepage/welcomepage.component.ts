import { Component, OnInit } from '@angular/core';
import {GetTruckDetailsService} from "../services/get-truck-details.service"
import {Router} from "@angular/router";
import { NgxSpinnerService } from 'ngx-spinner';
@Component({
  selector: 'app-welcomepage',
  templateUrl: './welcomepage.component.html',
  styleUrls: ['./welcomepage.component.css']
})
export class WelcomepageComponent implements OnInit {
  deviceDetails: any;
  bookingId:any;
  isTruckNotStarted: boolean;
  isNumNotCorrect: boolean;
  isNotStarted: boolean;
  values: string;
  isCancelled: boolean;
  constructor(private service:GetTruckDetailsService,private route:Router,private spinner:NgxSpinnerService) { }

  ngOnInit() {
  }
  onKey(event: any) { // without type info
    this.values = event.target.value;
    console.log(this.values);
    if(this.values.length == 0){
      this.isNumNotCorrect=false; 
      this.isNotStarted=false;  
      this.isCancelled = false;   
    }
      if (event.keyCode === 13) {
       event.preventDefault();
       document.getElementById("myBtn").click();
      }
  }
  getDeviceDetails(bookingId){
    debugger;
    this.spinner.show();
    this.service.getTruckdetails(bookingId).subscribe(data=>{
      this.deviceDetails=data;
      console.log(this.deviceDetails);
    if(this.deviceDetails.responseCode == "200"){
      localStorage.setItem('deviceDetails', JSON.stringify(this.deviceDetails));
      localStorage.setItem('DESTLAT', JSON.stringify(this.deviceDetails.truckDetails.destinationLattitude));
    localStorage.setItem('DESTLON', JSON.stringify(this.deviceDetails.truckDetails.destinationLongitude));
    localStorage.setItem('lastUpdatedLAT', JSON.stringify(this.deviceDetails.truckDetails.lastUpdatedLatitude));
    localStorage.setItem('lastUpdatedLON', JSON.stringify(this.deviceDetails.truckDetails.lastUpdatedLongitude));
    }
  
      if(this.deviceDetails.responseCode == "200"){
        this.route.navigateByUrl("/home");
      }else  if(this.deviceDetails.responseCode == 1 && this.deviceDetails.errorMessage === "Your Trip was cancelled"){
        this.isNumNotCorrect=false;   
        this.isNotStarted=false;
        this.isCancelled = true;              
      } 
      else  if(this.deviceDetails.responseCode == 1 && this.deviceDetails.errorMessage === "please enter correct bookingId/mrn"){
          this.isNumNotCorrect=true;   
          this.isNotStarted=false;  
          this.isCancelled = false;            
      } 
    if(this.deviceDetails.errorMessage === "Driver didn't start the pickup" && this.deviceDetails.responseCode == 1){
          this.isNotStarted=true;
          this.isNumNotCorrect=false;  
          this.isCancelled = false;             
      }
   
    }, error => {
    });
    this.spinner.hide();
   
  } 
}
