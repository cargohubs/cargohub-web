import { TestBed } from '@angular/core/testing';

import { GetTruckDetailsService } from './get-truck-details.service';

describe('GetTruckDetailsService', () => {
  beforeEach(() => TestBed.configureTestingModule({}));

  it('should be created', () => {
    const service: GetTruckDetailsService = TestBed.get(GetTruckDetailsService);
    expect(service).toBeTruthy();
  });
});
