import { Component, OnInit } from '@angular/core';
import {GetTruckDetailsService} from "../services/get-truck-details.service";
import { NgxSpinnerService } from 'ngx-spinner';
declare var google: any;

@Component({
  selector: 'app-home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.css']
})

export class HomeComponent implements OnInit {
   bookingId:any;
  
   details: any;
   allLocations:any = [];
   destLat:any;
   destLon:any;
   latUpdtLat:any;
   latUpdtLon:any;
    image;
  deviceDetails: any;
  values: any;
  constructor(private service:GetTruckDetailsService,private spinner:NgxSpinnerService) { 
    debugger;
  }

  ngOnInit(){
    debugger;
    this.details=JSON.parse(localStorage.getItem('deviceDetails'));
    console.log(this.details);
    this.destLat =JSON.parse(localStorage.getItem('DESTLAT'));
    this.destLon = JSON.parse(localStorage.getItem('DESTLON'));
    this.latUpdtLat =JSON.parse(localStorage.getItem('lastUpdatedLAT'));
    this.latUpdtLon = JSON.parse(localStorage.getItem('lastUpdatedLON'));

    this.trucklocations();
  }
  onKey(event: any) { // without type info
    this.values = event.target.value;
    console.log(this.values);
      if (event.keyCode === 13) {
       event.preventDefault();
       document.getElementById("myBtn").click();
      }
  }


  trucklocations(){
    debugger;
   let locations = [
      ['Destination',this.destLat ,this.destLon , 1],
      ['last updated', this.latUpdtLat, this.latUpdtLon, 2],
    ]

    let map = new google.maps.Map(document.getElementById('map'), {
      zoom: 4,
      center: new google.maps.LatLng(52.136,5.2913),
      mapTypeId: google.maps.MapTypeId.ROADMAP
    });
  
    var infowindow = new google.maps.InfoWindow();
    var bounds = new google.maps.LatLngBounds();
    for (i = 0; i<locations.length; i++) {  
      if(i == 0){
        this.image=" ../../assets/images/w3.png";
      } if(i == 1){
        this.image=" ../../assets/images/truck_40 X 40.png";
      }
     
      var marker, i;
      marker = new google.maps.Marker(
        {
        position: new google.maps.LatLng(locations[i][1], locations[i][2]),
        map: map,
       icon: this.image
      });
      var latlong = new google.maps.LatLng(locations[i][1], locations[i][2]);

      bounds.extend(latlong);
      google.maps.event.addListener(marker, 'click', (function(marker, i) {
        return function() {
          infowindow.setContent(this.locations[i][0]);
          infowindow.open(map, marker);
        }
      })(marker, i));
    }
    map.fitBounds(bounds);
  }
  getDeviceDetails(bookingId){
  this.spinner.show();
    this.service.getTruckdetails(bookingId).subscribe(data=>{
      this.deviceDetails = data;
      
      console.log( this.deviceDetails);
      if(this.deviceDetails.responseCode == "200"){
        localStorage.setItem('deviceDetails', JSON.stringify(this.deviceDetails));
        localStorage.setItem('DESTLAT', JSON.stringify(this.deviceDetails.truckDetails.destinationLattitude));
      localStorage.setItem('DESTLON', JSON.stringify(this.deviceDetails.truckDetails.destinationLongitude));
      localStorage.setItem('lastUpdatedLAT', JSON.stringify(this.deviceDetails.truckDetails.lastUpdatedLatitude));
      localStorage.setItem('lastUpdatedLON', JSON.stringify(this.deviceDetails.truckDetails.lastUpdatedLongitude));
      this.ngOnInit();
      }
      if(this.deviceDetails.errorMessage === "Driver didn't start the pickup" && this.deviceDetails.responseCode == 1){
             alert("Driver yet to start to pickup address")
      }else if(this.deviceDetails.responseCode == 1 && this.deviceDetails.errorMessage === "Your Trip was cancelled"){
        alert("Your Trip was cancelled"); 
     } 
      else if(this.deviceDetails.responseCode == 1 && this.deviceDetails.errorMessage === "please enter correct bookingId/mrn"){
              alert("Please enter valid details"); 
      } 
    }, error => {
    });
    this.spinner.hide();
  }
}
